## Small front end projects helping me to learn some basic Vanilla JS

Projects constructed on basis of Brad Traversy's course

## Project Specifications

- Create Layouts & UI's With HTML/CSS ( No CSS Frameworks )

- CSS Animations (Transitions, Keyframes, etc With JS Triggers)

- JavaScript Fundamentals

- DOM Selection & Manipulation

- JavaScript Events (Forms, buttons, scrolling, etc)

- Fetch API & JSON

- HTML5 Canvas

- The Audio & Video API

- Drag & Drop

- Web Speech API (Syth & Recognition)

- Working with Local Storage

- High Order Array Methods - forEach, map, filter, reduce, sort

- setTimout, setInterval

- Arrow Functions
 
